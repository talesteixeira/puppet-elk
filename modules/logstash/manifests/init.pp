class logstash {

yumrepo { 'logstash':
  ensure         => 'present',
  baseurl        => 'http://packages.elastic.co/logstash/2.2/centos',
  descr          => 'Logstash repository for 2.2.x packages',
  enabled        => '1',
  failovermethod => 'priority',
  gpgcheck       => '1',
  gpgkey         => 'http://packages.elastic.co/GPG-KEY-elasticsearch',
}

->

package {'logstash':
  ensure => present,
}

->

file {'/etc/logstash/conf.d/main.conf': 
   ensure => file,
   mode   => '0644',
   owner  => 'root',
   group  => 'root',
   source => 'puppet:///modules/logstash/main.conf',
 }

~>

service { 'logstash':
  ensure => 'running',
  enable => 'true',
}

}
