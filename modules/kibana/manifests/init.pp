class kibana ($kibana_version = '4.4.2') {

user { 'kibana':
  ensure           => 'present',
  shell		   => '/bin/false',
}

-> 

staging::deploy { "kibana-${kibana_version}-linux-x64.tar.gz":
  source => "https://download.elastic.co/kibana/kibana/kibana-${kibana_version}-linux-x64.tar.gz",
  target => '/opt',
}


file {"/opt/kibana-${kibana_version}-linux-x64/config/kibana.yml":
        ensure  => file,
        source  => 'puppet:///modules/kibana/kibana.yml',
        }

file {'/usr/lib/systemd/system/kibana.service':
	ensure	=> file,
	mode	=> '0644',
	owner	=> 'root',
	group	=> 'root',
	source	=> 'puppet:///modules/kibana/kibana.service',
	}

->

service {'kibana': 
  ensure => running,
  enable => true,
   }
}
