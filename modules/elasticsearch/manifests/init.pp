class elasticsearch {

yumrepo { 'elasticsearch':
  ensure         => 'present',
  baseurl        => 'https://packages.elastic.co/elasticsearch/2.x/centos',
  descr          => 'Elasticsearch repository for 2.x packages',
  enabled        => '1',
  failovermethod => 'priority',
  gpgcheck       => '1',
  gpgkey         => 'https://packages.elastic.co/GPG-KEY-elasticsearch',
}

->

package {'elasticsearch':
  ensure => present,
}

~>

service { 'elasticsearch':
  ensure => 'running',
  enable => 'true',
}

}
