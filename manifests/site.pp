node 'centos7.puppet' {
    class { 'java':
   	distribution => 'jre',
        before 	     => [Class['::elasticsearch'], Class['::logstash']],
	}

   class {'::elasticsearch': }
   class {'::logstash': }
   include ::kibana
	
}
